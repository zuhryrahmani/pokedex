// main
/** @jsxImportSource @emotion/react */
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { jsx } from '@emotion/react';

// asses
import logo from '../../assets/icons/pokemon-yellow.svg';

const Count = ({count=0}) => {

  const history = useHistory();
  const breakpoints = [576, 768, 992, 1200];
  const mw = breakpoints.map(bp => `@media (max-width: ${bp}px)`);
  const countStyle = {
    width: 70,
    height: 70,
    background: `url(${logo}) no-repeat`,
    backgroundSize: 'cover',
    lineHeight: '77px',
    textAlign: 'center',
    color: '#2A3050',
    fontWeight: 900,
    boxSizing: 'border-box',
    float: 'right',
    position: '-webkit-sticky',
    position: 'sticky',
    top: 20,
    zIndex: 1,
    filter: 'drop-shadow(0 0 8px rgba(0,0,0,0.7))',
    margin: '-20px -15px 0 0',
    cursor: 'pointer',
    [mw[0]]: {
      position: 'fixed',
      right: 20
    }
  };

  return <div css={countStyle} onClick={() => history.push('/my-pokemon-list')}>{count}</div>;
};

export default Count;
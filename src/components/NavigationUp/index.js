// main
/** @jsxImportSource @emotion/react */
import React, { useEffect } from 'react';
import { jsx } from '@emotion/react';
import { Fab, Slide } from '@material-ui/core';
import { NavigationRounded } from '@material-ui/icons';

const NavigationUp = ({scroll=0}) => {


  const breakpoints = [576, 768, 992, 1200];
  const mw = breakpoints.map(bp => `@media (max-width: ${bp}px)`);
  const fab = {
    '&.MuiButtonBase-root': {
      position: 'fixed',
      bottom: 110,
      right: 30,
      [mw[0]]: {
        bottom: 100,
        right: 20,
      }
    }
  };

  return(
    <Slide direction='up' in={scroll >= 100} mountOnEnter unmountOnExit>
      <Fab color='secondary' size='medium' css={fab} onClick={() => document.scrollingElement.scrollTop = 0}>
        <NavigationRounded />
      </Fab>
    </Slide>
  );
};

export default NavigationUp;